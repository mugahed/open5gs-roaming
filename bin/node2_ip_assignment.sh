#!/bin/bash

set -ex

interface=$(ip addr | awk -vtarget_addr=10.10.2.5 '
/^[0-9]+/ {
  iface=substr($2, 0, length($2)-1)
}

$1 == "inet" {
  split($2, addr, "/")
  if (addr[1] == target_addr) {
    print iface
  } 
}
')


ip addr add 10.10.2.252/16 dev $interface
ip addr add 10.10.2.251/16 dev $interface
