#!/bin/bash

set -ex

cd /var/tmp/open5gs

tmux new-session -d -s my_session
tmux split-window -t my_session:0 -v
tmux split-window -t my_session:0 -v

tmux new-window -t my_session  # Create second window
tmux split-window -t my_session:1 -v
tmux split-window -t my_session:1 -v

tmux new-window -t my_session  # Create third window
tmux split-window -t my_session:2 -v
tmux split-window -t my_session:2 -v

tmux new-window -t my_session  # Create forth window
tmux split-window -t my_session:3 -v
tmux split-window -t my_session:3 -v

tmux new-window -t my_session  # Create fifth window
tmux split-window -t my_session:4 -v

tmux send-keys -t "my_session:0.0" 'sudo ./install/bin/open5gs-nrfd -c /local/repository/etc/node1/nrf.yaml.in' Enter
tmux send-keys -t "my_session:0.1" 'sudo ./install/bin/open5gs-scpd -c /local/repository/etc/node1/scp.yaml.in' Enter
tmux send-keys -t "my_session:0.2" 'sudo ./install/bin/open5gs-seppd -c /local/repository/etc/node1/sepp.yaml' Enter

tmux send-keys -t "my_session:1.0" 'sudo ./install/bin/open5gs-amfd -c /local/repository/etc/node1/amf.yaml.in' Enter
tmux send-keys -t "my_session:1.1" 'sudo ./install/bin/open5gs-smfd -c /local/repository/etc/node1/smf.yaml.in' Enter
tmux send-keys -t "my_session:1.2" 'sudo ./install/bin/open5gs-upfd -c /local/repository/etc/node1/upf.yaml.in' Enter

tmux send-keys -t "my_session:2.0" 'sudo ./install/bin/open5gs-ausfd -c /local/repository/etc/node1/ausf.yaml.in' Enter
tmux send-keys -t "my_session:2.1" 'sudo ./install/bin/open5gs-udmd -c /local/repository/etc/node1/udm.yaml.in' Enter
tmux send-keys -t "my_session:2.2" 'sudo ./install/bin/open5gs-pcfd -c /local/repository/etc/node1/pcf.yaml.in' Enter

tmux send-keys -t "my_session:3.0" 'sudo ./install/bin/open5gs-nssfd -c /local/repository/etc/node1/nssf.yaml.in' Enter
tmux send-keys -t "my_session:3.1" 'sudo ./install/bin/open5gs-bsfd -c /local/repository/etc/node1/bsf.yaml.in' Enter
tmux send-keys -t "my_session:3.2" 'sudo ./install/bin/open5gs-udrd -c /local/repository/etc/node1/udr.yaml.in' Enter

tmux send-keys -t "my_session:4.0" 'sudo ~/PacketRusher/packetrusher --config /local/repository/etc/packetRusher/node1_config.yml ue' Enter

