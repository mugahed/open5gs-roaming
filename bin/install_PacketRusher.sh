#!/bin/bash

set -ex


sudo apt install -y  build-essential linux-headers-generic make git wget tar linux-modules-extra-$(uname -r)


# Warning this command will remove your existing local Go installation if you have one
sudo wget https://go.dev/dl/go1.21.3.linux-amd64.tar.gz && sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.21.3.linux-amd64.tar.gz


export PATH=$PATH:/usr/local/go/bin


source $HOME/.profile


git clone https://github.com/HewlettPackard/PacketRusher  ~/PacketRusher # or download the ZIP from https://github.com/HewlettPackard/PacketRusher/archive/refs/heads/master.zip and upload it to your Linux server
cd ~/PacketRusher && echo "export PACKETRUSHER=$PWD" >> $HOME/.profile && source $HOME/.profile


cd $PACKETRUSHER/lib/gtp5g
make clean && make && sudo make install


cd $PACKETRUSHER
go mod download
go build cmd/packetrusher.go
./packetrusher --help



