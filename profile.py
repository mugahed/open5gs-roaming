import os
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.urn as URN
import geni.rspec.emulab.pnext as PN


pc = portal.Context()

node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
    ("m510", "Emulab, m510"),
    ("xl170", "Emulab, xl170"),
]


node_os = [ "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD", "urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS8S-64-STD", "urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS9S-64-STD"]


DEFAULT_SRS_HASHES = {
    "srsRAN_4G": "release_23_04_1",
    "srsRAN_Project": "release_23_5",
}


pc.defineParameter(
    name="node1type",
    description="Type of compute node to used for node 1",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types,
)
pc.defineParameter(
    name="node2type",
    description="Type of compute node to used for node 2",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types,
)


pc.defineParameter(
    name="node1_ostype",
    description="Type of OS to used for node 1",
    typ=portal.ParameterType.STRING,
    defaultValue=node_os[0],
    legalValues=node_os,
)

pc.defineParameter(
    name="node2_ostype",
    description="Type of OS to used for node 2",
    typ=portal.ParameterType.STRING,
    defaultValue=node_os[0],
    legalValues=node_os,
)




params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()




node = request.RawPC("node")
node.hardware_type = params.node1type
node.disk_image = params.node1_ostype
node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/deploy-open5gs.sh"))
node.addService(rspec.Execute(shell="bash", command="sudo cp /local/repository/etc/node1/* /var/tmp/open5gs/configs/open5gs/"))
node.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/node1_ip_assignment.sh"))
node.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/edit_hosts_node1.sh"))
node.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/install_PacketRusher.sh"))
node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/run_core_tmux_node1.sh"))
for srs_type, type_hash in DEFAULT_SRS_HASHES.items():
    cmd = "{} '{}' {}".format("sudo /local/repository/bin/deploy-srs.sh", type_hash, srs_type)
    node.addService(rspec.Execute(shell="bash", command=cmd))

node.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/install-user-node1.sh"))




node2 = request.RawPC("node2")
node2.hardware_type = params.node2type
node2.disk_image = params.node2_ostype
node2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/deploy-open5gs.sh"))
node2.addService(rspec.Execute(shell="bash", command="sudo cp /local/repository/etc/node2/* /var/tmp/open5gs/configs/open5gs/"))
node2.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/node2_ip_assignment.sh"))
node2.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/edit_hosts_node2.sh"))
node2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/run_core_tmux_node2.sh"))
for srs_type, type_hash in DEFAULT_SRS_HASHES.items():
    cmd = "{} '{}' {}".format( "sudo /local/repository/bin/deploy-srs.sh",type_hash, srs_type)
    node2.addService(rspec.Execute(shell="bash", command=cmd))

node2.addService(rspec.Execute(shell="bash", command="sudo /local/repository/bin/install-user-node2.sh"))






iface1 = node.addInterface("to_operator_B")
#iface1.component_id = "eth1"
iface1.addAddress(rspec.IPv4Address("10.10.1.5", "255.255.0.0"))
#iface1.addAddress(rspec.IPv4Address("10.10.1.251", "255.255.0.0"))
#iface1.addAddress(rspec.IPv4Address("10.10.1.252", "255.255.0.0"))


iface2 = node2.addInterface("to_operator_A")
#iface2.component_id = "eth1"
iface2.addAddress(rspec.IPv4Address("10.10.2.5", "255.255.0.0"))
#iface2.addAddress(rspec.IPv4Address("10.10.2.251", "255.255.0.0"))
#iface2.addAddress(rspec.IPv4Address("10.10.2.252", "255.255.0.0"))


link = request.LAN("lan")
link.addInterface(iface1)
link.addInterface(iface2)

pc.printRequestRSpec(request)
